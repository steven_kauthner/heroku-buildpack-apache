#NB, this doesn't seem to work. I got apache set up alright and configured as a forward proxy, but I don't think proxy requests reach the actual dyno, probably heroku's routing mesh gets in the way

#These instructions are used to create the apache software used by this buildpack. Compile apache on a heroku dyno as directed below, then upload it to S3, from where this buildpack will download it

#First create a temporary heroku app, then on your local machine do
heroku run bash

#Then use this environment to download and compile apache
curl -O http://mirror.olnevhost.net/pub/apache//httpd/httpd-2.4.12.tar.gz
curl -O http://mirrors.advancedhosters.com/apache//apr/apr-1.5.2.tar.gz
curl -O http://mirrors.advancedhosters.com/apache//apr/apr-util-1.5.4.tar.gz

tar -xzf httpd-2.4.12.tar.gz
tar -xzf apr-1.5.2.tar.gz && mv apr-1.5.2 httpd-2.4.12/srclib/apr
tar -xzf apr-util-1.5.4.tar.gz && mv apr-util-1.5.4 httpd-2.4.12/srclib/apr-util
cd httpd-2.4.12

./configure --prefix=/app/apache --with-included-apr --enable-mods-shared='all' --enable-proxy=shared --enable-proxy-connect=shared --enable-proxy-http=shared --enable-proxy-ftp=shared --enable-proxy-scgi=shared --enable-proxy-ajp=shared --enable-proxy-balancer=shared && make && make install

#Then package the compiled software up
cd ../
tar czf apache-2.4.12.tar.gz -C /app apache

#And get it off the heroku dyno by downloading from your browser
curl -O https://raw.githubusercontent.com/scottmotte/srvdir-binary/master/srvdir.tar.gz
tar -zxf srvdir.tar.gz
./srvdir	#(this command will display a URL, paste it into the browser, and append apache-2.4.12.tar.gz to it)
